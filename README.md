<h3>Make Morrowind Animation Easier</h3>  
  
For use with [Greatness7's morrowind IO addon](https://blender-morrowind.readthedocs.io/en/latest/).  
This addon simply takes the result of importing a .nif file with that addon, and processes it for ease-of-editing, then puts the file  
back into the original format for easy export.  
  
It can (after importing a model with animation using the Morrowind IO addon by Greatness7):  
Process Import:  
  chops up Morrowind's animations into Blender Actions for easily editing.  
Prepare Export:  
  This takes the Actions from their easily editable form and puts them back into the form the Morrowind exporter expects.  
Swap Morrowind Animation:  
  This sets the objects in the scene to use the editable actions made with Process Import, so that the animator can easily edit all the objects that are animated.  
  
  
I threw this together in a few hours because I felt like trying to do a creature replacer and felt that the animation process would be too tedious without something to help me. Because it is kinda thrown-together, it's not very fancy or pretty, but it works and if there are bugs, let me know and I will probably fix them. Probably! 
  
<h5>Installation Instructions</h5>  
Just download this repo as a .zip and install the usual way. Inside Blender, Edit>Prefereneces>Addons and click the install button at the top right. Then choose the .zip folder.  
Or, just copy the mmae.py file into your Blender config folder.  
