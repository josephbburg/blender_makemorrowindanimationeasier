bl_info = {
    "name": "Make Morrowind Animation Easier",
    "description": "",
    "author": "Node Spaghetti",
    "version": (0, 0),
    "blender": (2, 93, 0),
    "location": "search (F3) ",
    "warning": "",
    "tracker_url": "H",
    "category": "Animation",
}

# Animation Groups
animation_groups = ["Idle", "Idle2", "Idle3", "Idle4", "Idle5",
                    "Idle6", "Idle7", "Idle8", "Idle9", "Idlehh",
                    "Idle1h", "Idle2c", "Idle2w", "IdleSwim",
                    "IdleSpell", "IdleCrossbow", "IdleSneak",
                    "IdleStorm", "Torch", "Hit1", "Hit2", "Hit3",
                    "Hit4", "Hit5", "SwimHit1", "SwimHit2", "SwimHit3",
                    "Death1", "Death2", "Death3", "Death4", "Death5",
                    "DeathKnockDown", "DeathKnockOut", "KnockDown",
                    "KnockOut", "SwimDeath", "SwimDeath2", "SwimDeath3",
                    "SwimDeathKnockDown", "SwimDeathKnockOut",
                    "SwimKnockOut", "SwimKnockDown", "SwimWalkForward",
                    "SwimWalkBack", "SwimWalkLeft", "SwimWalkRight",
                    "SwimRunForward", "SwimRunBack", "SwimRunLeft",
                    "SwimRunRight", "SwimTurnLeft", "SwimTurnRight",
                    "WalkForward", "WalkBack", "WalkLeft", "WalkRight",
                    "TurnLeft", "TurnRight", "RunForward", "RunBack",
                    "RunLeft", "RunRight", "SneakForward", "SneakBack",
                    "SneakLeft", "SneakRight", "Jump", "WalkForwardhh",
                    "WalkBackhh", "WalkLefthh", "WalkRighthh", 
                    "TurnLefthh", "TurnRighthh", "RunForwardhh", 
                    "RunBackhh", "RunLefthh", "RunRighthh", 
                    "SneakForwardhh", "SneakBackhh", "SneakLefthh",
                    "SneakRighthh", "Jumphh", "WalkForward1h",
                    "WalkBack1h", "WalkLeft1h", "WalkRight1h",
                    "TurnLeft1h", "TurnRight1h", "RunForward1h",
                    "RunBack1h", "RunLeft1h", "RunRight1h", 
                    "SneakForward1h", "SneakBack1h", "SneakLeft1h", 
                    "SneakRight1h", "Jump1h", "WalkForward2c", 
                    "WalkBack2c", "WalkLeft2c", "WalkRight2c", 
                    "TurnLeft2c", "TurnRight2c", "RunForward2c", 
                    "RunBack2c", "RunLeft2c", 
                    "RunRight2c", "SneakForward2c", "SneakBack2c",
                    "SneakLeft2c", "SneakRight2c", "Jump2c",
                    "WalkForward2w", "WalkBack2w", "WalkLeft2w",
                    "WalkRight2w", "TurnLeft2w", "TurnRight2w",
                    "RunForward2w", "RunBack2w", "RunLeft2w",
                    "RunRight2w", "SneakForward2w", "SneakBack2w",
                    "SneakLeft2w", "SneakRight2w", "Jump2w",
                    "SpellCast", "SpellTurnLeft", "SpellTurnRight",
                    "Attack1", "Attack2", "Attack3", "SwimAttack1",
                    "SwimAttack2", "SwimAttack3", "HandToHand",
                    "Crossbow", "BowAndArrow", "ThrowWeapon",
                    "WeaponOneHand", "WeaponTwoHand", "WeaponTwoWide",
                    "Shield", "PickProbe", "InventoryHandToHand",
                    "InventoryWeaponOneHand", "InventoryWeaponTwoHand",
                    "InventoryWeaponTwoWide",
                    #these aren't animation groups, really...
                    #  but I have to treat them as such because Morrowind
                    #  has dumb inconsistencies...
                    "SpellCast: Unequip", "SpellCast: Self",
                    "SpellCast: Target", "SpellCast: Touch",
                    "SpellCast: Equip",]


for i, string in enumerate(animation_groups):
    animation_groups[i] = string.casefold()
    # ugly hack to prevent letter case from mattering
    


from math import ceil
import bpy
from mathutils import Vector


# Morrowind defines its actions in text keys in the .NIF files
# The Morrowind Importer then turns them into a Pose Library with
#  Pose Markers at the correct frames
# There are four textkeys that define animation timing: 
#   - "AnimName: Start",
#   - "AnimName: Stop",
#   - "AnimName: Loop Start",
#   - "AnimName: Loop Stop"
# Now, the AnimName bit is different for every animation...
# EXCEPT! Any animation may have any number of SoundGen text keys
#  at any point in the animation
#
# I think I am gonna make custom fCurves for the SoundGen stuff, so
#  I can more easily deal with 'em in the normal animation workflow
# I think it's also possible to make Blender play the sounds using
#  an addon and event handlers, but that's overkill
#
# It looks like a "Hit" key specifies when attacks connect.

# it would be nice to make a generic method for this, but
#  friggin' morrowind is so inconsistent that I have to hardcode

def find_anim_grp(pm_name):
    matches = []
    for anim_grp in animation_groups: # and find if the animation
        if anim_grp in pm_name: # group is in the string.
            matches.append(anim_grp        )
    # if there's no animation group, check for SoundGen
    if matches:
        matches.sort()
        anim_grp = matches[-1] # choose the biggest one.
    else:
        if 'soundgen' in pm_name:
            anim_grp = 'soundgen'
        else:
            print ("Pose Marker: ", pm_name, " does not appear to be a Morrowind Text Key.")
            return None
    return anim_grp



def ParseTextKeysFromAction(context, action):
    # UNfortunately humanoid animations are stupid and inconsistent and
    #  have multiple start/end keys or keys for equipping and combat
    #  since I can't count on every action to have start/end...
    
    # I have to start by sorting the pose markers so I can seek them.
    # I'm gonna sort 'em by frame and put the frames in a dict.
    # I don't expect to find multiple keys on one frame very often
    # I can sort each list into the right order:
    #  Loop Stop, Stop, Start, Loop Start, Sound, Other
    # At this point, I can just tackle 'em in order
    
    # Instead of caring about 'Start' and 'Stop', only watch for when
    #  an animation group changes
    # The goal is to preserve the keys exactly as they are, so...
    # The start frame will be whatever the first text key is for the
    #  animation, and they'll have to be in order.
    
    pose_markers = {}
    for pm in action.pose_markers: # Iterate through the animation groups
        if ((frame := pose_markers.get(pm.frame, None)) is None):
            pose_markers[pm.frame] = []
        # looks like sometimes the key is 'sound' instead of 'soundgen'!! ARGGHHHH!!!
        text_key = pm.name.casefold().strip()
        if text_key == 'sound':
            text_key = 'soundgen'
        pose_markers[pm.frame].append(text_key)
        # we have frame and name, all we need.
        # Deal with caps and leading/trailing whitespace now
    # gotta sort the lists
    def sort_pose_markers(pm):
        grp = pm.split(': ')[0]; tk  = pm.split(': ')[1]
        #  and this is how we know if we have a 'Start' or a 'Loop Start'
        if (grp == 'soundgen'):
            return 12
        if tk == 'loop stop':
            return 2
        if tk == 'stop':
            return 4
        if tk == 'start':
            return 8        
        if tk == ' loop start':
            return 10
        return 15 # other stuff
    
    
    # however: sorting them is also not enough since animations can
    #  overlap. Because of this, we need to track when the 
    
    animations = {}
    for frame in range(context.scene.frame_end+1):# +1 bc zero-indexed
        markers = pose_markers.get(frame, None)
        if (not markers):
            continue #nothing on this frame
        if (len(markers) > 1):  # now sort the lists:
            markers.sort(key=sort_pose_markers)
        for pm in markers:
            grp = pm.split(': ')[0]; tk  = pm.split(': ')[1]
            if ((text_keys := animations.get(grp, None)) is None):
                animations[grp] = {'start_frame':frame, tk:frame, 'end_frame':frame}
            else:
                animations[grp]['end_frame'] = frame
                animations[grp][tk] = frame
                # this should track when each animation starts and ends
    
                
                
        
        
        
        
        
    
    return animations

def GrabKeyInRange(context, action_from, action_to, ob, start, end, offset=0, do_end = True):
    # grabs the keys in a range, also gets the initial pose by creating the keys it needs to
    #  for this, I can just seek the animation and record everything at the start/end frames
    # start by seeking to the start point and making a start key
    #
    
    def keys_at_frame(id, object_animation = False):
        if (id):
            grp = id.name
            if object_animation:
                grp = "Object Transforms"
            ignore_object_transforms = False
            try:
                if ( ((id.type == 'MESH') or (id.type == 'EMPTY')) and ob.parent.type == 'ARMATURE'):
                    ignore_object_transforms = True
            except AttributeError: # there's no parent or it's a pose bone
                pass
                    
            for prop_array in ['location', 'rotation_quaternion', 'scale', 'hide_viewport']:
                if (ignore_object_transforms == True and prop_array != 'hide_viewport'):
                    continue
                for i in range(4):
                    if (prop_array != 'rotation_quaternion') and (i > 2):
                        continue
                    if (prop_array == 'hide_viewport'):
                        # also do visibility, lotsa hacky stuff here, oh well.
                        if not hasattr(id, prop_array):
                            continue
                        if i > 0:
                            continue
                        if ((fc:= action_to.fcurves.find(id.path_from_id(prop_array), index=i) ) is None):
                            fc = action_to.fcurves.new(id.path_from_id(prop_array), index=i, action_group=grp)
                        fc.keyframe_points.insert(context.scene.frame_current + offset, getattr(id, prop_array), options={'FAST'})
                        continue
                    if ((fc:= action_to.fcurves.find(id.path_from_id(prop_array), index=i)) is None):
                        fc = action_to.fcurves.new(id.path_from_id(prop_array), index=i, action_group=grp)
                    kf = fc.keyframe_points.insert(context.scene.frame_current + offset, getattr(id, prop_array)[i], options={'FAST'})
        else: # for now, I will assume this means this is hidden
            if ((fc:= action_to.fcurves.find("hide_viewport") ) is None):
                fc = action_to.fcurves.new("hide_viewport", action_group="Object Transforms")
            fc.keyframe_points.insert(context.scene.frame_current + offset, 1, options={'FAST'}) #hide==1 means hidden
            # annoyingly, this creates empty fcurves for the other Object Transforms stuff, without grouping them....
            for fc in action_to.fcurves:
                if fc.group is None:
                    for prop in ['location', 'rotation_quaternion', 'rotation_euler', 'scale']:
                        if prop in fc.data_path:
                            fc.group = action_to.groups['Object Transforms']
    # get the starting state:
    context.scene.frame_current = start
    dg = context.evaluated_depsgraph_get()
    ob_updated = dg.objects.get(ob.name)
    keys_at_frame(ob_updated, object_animation = True) # object animation
    if (ob.type == 'ARMATURE'): # bone animation
        for pb in ob_updated.pose.bones:
            keys_at_frame(pb)
    
    # now copy keys in the range...
    for fc_from in action_from.fcurves:
        if (ob.type != 'ARMATURE') and ("pose.bones" in fc_from.data_path):
            continue
        fc_to = action_to.fcurves.find(fc_from.data_path, index=fc_from.array_index) #should have just created this in the last step...
        if not (fc_to):
            fc_to = action_to.fcurves.new(fc_from.data_path, index=fc_from.array_index)
        for kf in fc_from.keyframe_points:
            if (kf.co[0] < start) or (kf.co[0] > end):
                continue
            new_kf = fc_to.keyframe_points.insert(kf.co[0]+offset, kf.co[1], options={'FAST'})
            for prop in ['co', 'handle_left', 'handle_left_type',
                         'handle_right', 'handle_right_type', 'interpolation',]:
                setattr(new_kf, prop, getattr(kf, prop))
                if prop in ['co', 'handle_left', 'handle_right']:
                    setattr(new_kf, prop, getattr(new_kf, prop) + Vector((offset, 0)))
            # this is kinda stupid
    # finally, get the ending state.
    if (do_end):
        context.scene.frame_current = end
        dg = context.evaluated_depsgraph_get()
        ob_updated = dg.objects.get(ob.name)
        keys_at_frame(ob_updated, object_animation = True) # object animation
        if (ob.type == 'ARMATURE'): # bone animation
            for pb in ob_updated.pose.bones:
                keys_at_frame(pb)
        
            
def GrabShapeKeyAnimationInRange(context, action_from, action_to, ob, start, end, offset=0):
    #IGNORE shape keys here, do 'em separately I guess
    
    # get shapekey states
    # unfortunately shapekeys suck and make everything much mroe complex
    # for whatever reason, this has to be done in a separate action
    
    
    keys = ob.data.shape_keys # calling block will ensure this is not None
    
    def keys_at_frame():
        for i, sk in enumerate(keys.key_blocks):
            if (i == 0):
                continue # first sk is Basis
            # create if not already there...
            data_path = "key_blocks[\""+sk.name+"\"].value"
            if ((fc:= action_to.fcurves.find(data_path, index=0)) is None):
                fc = action_to.fcurves.new(data_path, index=0, action_group=keys.name)
            fc.keyframe_points.insert(start + offset, sk.value, options={'FAST'})
    # get starting state...
    context.scene.frame_current = start
    dg = context.evaluated_depsgraph_get()
    ob_updated = dg.objects.get(ob.name)
    keys_at_frame()
    # now copy keys
    for fc_from in action_from.fcurves:
        fc_to = action_to.fcurves.find(fc_from.data_path, index=fc_from.array_index) #should have just created this in the last step...
        for kf in fc_from.keyframe_points:
            if (kf.co[0] < start) or (kf.co[0] > end):
                continue
            new_kf = fc_to.keyframe_points.insert(kf.co[0]+offset, kf.co[1], options={'FAST'})
            for prop in ['co', 'handle_left', 'handle_left_type',
                         'handle_right', 'handle_right_type', 'interpolation',]:
                setattr(new_kf, prop, getattr(kf, prop))
                if prop in ['co', 'handle_left', 'handle_right']:
                    setattr(new_kf, prop, getattr(new_kf, prop) + Vector((offset, 0)))
            # this is kinda stupid
    context.scene.frame_current = end
    dg = context.evaluated_depsgraph_get()
    ob_updated = dg.objects.get(ob.name)
    keys_at_frame()




def MorrowindPoseLibraryToActions(context, action_start):
    print ("\nProcessing Morrowind import...")
    animations = ParseTextKeysFromAction(context, action_start)
    for anim_name, anim_keys in animations.items():
        print (anim_name)
        # create a new Action, check if one exists first..
        if (action:= bpy.data.actions.get(anim_name, None)):
            bpy.data.actions.remove(action) # delete it, should be OK to do
        action = bpy.data.actions.new(anim_name)
        action.use_fake_user = True # since we won't switch to it just yet!
        # deal with the non-functional text keys first
        # just make useless fcurves with keys at the points that keys exist
        #  these will be converted back to markers later
        action.groups.new("animation_text_keys"); action.groups.new("sound_text_keys")
        # and we'll also include groups for the pose bones later!
        
        #annoyingly, some animations have no 'Start' key but instead they have various Start keys
        
        
        start_frame = anim_keys['start_frame']
        end_frame = anim_keys['end_frame']
        for key, frame in anim_keys.items():
            grp = "animation_text_keys"
            if ("soundgen" in key.casefold()):
                grp = "sound_text_keys"
            if ((fc := action.fcurves.find(key)) is None):
                fc = action.fcurves.new(key, action_group=grp)
            fc.keyframe_points.insert(frame - start_frame, 1, options={'FAST'})
            
            # gonna go ahead and save pose markers
            pm =action.pose_markers.new(key)
            pm.frame = frame - start_frame
            
        GrabKeyInRange(context, action_start, action, context.active_object, start_frame, end_frame, offset=start_frame*-1)
        #Now shapekeys and object animation
        for ob in bpy.data.objects:
            if (ob.type == 'MESH') or (ob.type == 'EMPTY'):
                try:
                    shapekeys = ob.data.shape_keys
                except AttributeError: # just an empty
                    shapekeys = None
                if shapekeys is None: # then what about object animation?
                    if (ob.animation_data is None):
                        continue
                    if (ob.animation_data.action is None):
                        continue
                    # handle object animation
                    action_ob = bpy.data.actions.new(anim_name+ob.name)
                    action_ob.use_fake_user = True
                    # print ("processing object animation for ", ob.name)
                    # print (start_frame, end_frame)
                    GrabKeyInRange(context, ob.animation_data.action, action_ob, ob, start_frame, end_frame, offset=start_frame*-1)
                    continue
                if (action:= bpy.data.actions.get(anim_name+"_vertex_anim_"+ob.name, None)):
                   bpy.data.actions.remove(action)
                action_vert = bpy.data.actions.new(anim_name+"_vertex_anim_"+ob.name)
                action_vert.use_fake_user = True
                GrabShapeKeyAnimationInRange(context, shapekeys.animation_data.action, action_vert, ob, start_frame, end_frame, offset=start_frame*-1)
                
                
        
    # for animation start and stop, just make that the length of the new Action
    # we'll grab the keys in that part of the animation, but first we'll make sure we get any keys set waaaaay before
    # so that the initial state is correct
    return

class process_import(bpy.types.Operator):
    """Process Morrowind Import"""
    bl_idname = "object.mw_actions_process_import"
    bl_label = "Process Morrowind Import"
    bl_options = {"UNDO", "REGISTER"}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        MorrowindPoseLibraryToActions(context, context.active_object.animation_data.action)
        return {'FINISHED'}


def ActionsToMorrowindPoseLibrary(context):
    print ("\nPreparing Morrowind export...")
    action = bpy.data.actions.new(context.active_object.name+"_morrowind_animation_export")
    action.use_fake_user = True
    action.groups.new("animation_text_keys"); action.groups.new("sound_text_keys")
    mw_actions = {}
    context.active_object.animation_data.action = action
    for a in bpy.data.actions:
        for grp_name in animation_groups:
            if grp_name in a.name.casefold():
                if not mw_actions.get(grp_name, None):
                    mw_actions[grp_name] =[]
                mw_actions[grp_name].append(a)
    # now we have a dict of animation groups with the associated actions
    # now we're gonna sort the list of mw_actions by length
    mw_actions = list(mw_actions.values()) # don' need keys anymore...
    def sort_by_anim_length(actions):
        return ceil(actions[0].frame_range[1] - actions[0].frame_range[0])
    mw_actions.sort(reverse=False, key=sort_by_anim_length)
    frame_cur = 0
    for acts in mw_actions:
        text_key_frame_offset = frame_cur
        for act in acts:
            if (act.name.casefold() not in animation_groups):
                continue
            print (act.name, frame_cur)
            # make fcurves if necesary:
            for fc in act.fcurves:
                if (fc.group):
                    if ((fc.group.name in ["animation_text_keys", "sound_text_keys"])
                      and (action.fcurves.find(fc.data_path, index=fc.array_index) is None)):
                        action.fcurves.new(fc.data_path, index=fc.array_index, action_group=fc.group.name)
                    # these are not actually useful at this point, we're gonna add pose markers instead
            frame_range = ceil(act.frame_range.length) # for whatever reason, not always an int
            GrabKeyInRange(context, act, action, context.active_object, 0, frame_cur + frame_range, offset = frame_cur, do_end=False) # do_end seems to mess this up for some reason
            frame_cur += frame_range + 1
            # add pose markers:
            for fc in act.groups["animation_text_keys"].channels:
                pm = action.pose_markers.new(act.name+": "+fc.data_path); pm.frame = fc.keyframe_points[0].co[0] + text_key_frame_offset
            for fc in act.groups["sound_text_keys"].channels:
                pm = action.pose_markers.new(act.name+": "+fc.data_path); pm.frame = fc.keyframe_points[0].co[0] + text_key_frame_offset
                
                
    for fc in action.groups["animation_text_keys"].channels:
        action.fcurves.remove(fc)
    for fc in action.groups["sound_text_keys"].channels:
        action.fcurves.remove(fc)
    # now let's do shapekeys and object animations
    for ob in bpy.data.objects:
        if ((ob.type == 'MESH') or (ob.type == 'EMPTY')):
            try:
                shapekeys = ob.data.shape_keys
            except AttributeError: # not a mesh
                shapekeys = None
            if shapekeys is None: # do object animation:
                frame_cur = 0
                action_ob = bpy.data.actions.new(action.name+ob.name)
                action_ob.use_fake_user = True
                for acts in mw_actions:
                    for act in acts:
                        if ((ob.name not in act.name) or ("_vertex_anim" in act.name)):
                            continue
                        frame_range = ceil(act.frame_range.length) # for whatever reason, not always an int
                        GrabKeyInRange(context, act, action_ob, ob, 0, frame_cur + frame_range, offset = frame_cur, do_end=False)
                        frame_cur += frame_range + 1
                        try:
                            ob.animation_data.action = action_ob
                        except AttributeError:
                            pass # this is an empty object with no animation.
                
            
            # if (delete_me:= bpy.data.actions.get(shapekeys.name+'Action', None)):
                # bpy.data.actions.remove(delete_me) # delete it, should be OK to do
            frame_cur = 0
            action_vert = bpy.data.actions.new(action.name+"_vertex_anim_"+ob.name)
            action_vert.use_fake_user = True
            for acts in mw_actions:
                for act in acts:
                    if ((ob.name not in act.name) or ("_vertex_anim" not in act.name)):
                        continue
                    frame_range = ceil(act.frame_range.length) # for whatever reason, not always an int
                    GrabShapeKeyAnimationInRange(context, act, action_vert, ob, 0, frame_cur + frame_range, offset = frame_cur)
                    frame_cur += frame_range + 1
                    bpy.data.shape_keys[shapekeys.name].animation_data.action = action_vert
    bpy.context.scene.frame_start = 0
    bpy.context.scene.frame_end = ceil(action.frame_range[1])
        


class prepare_export(bpy.types.Operator):
    """Prepare Actions for export to Morrowind"""
    bl_idname = "object.mw_actions_prepare_export"
    bl_label = "Prepare Actions for export to Morrowind"
    bl_options = {"UNDO", "REGISTER"}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        ActionsToMorrowindPoseLibrary(context, )
        return {'FINISHED'}


class mw_swap_animation(bpy.types.Operator):
    """Macro for switching bone and shapekey animations all at once"""
    bl_idname = "object.mw_swap_animation"
    bl_label = "Swap Morrowind Animation"
    bl_options = {"UNDO", "REGISTER"}
    
    def populate_animation_list(self, context):
        animations = []
        i = 0
            #this way, selected objects are higher on the list
        #first make sure the active animation is on top
        for a in bpy.data.actions:
            if ("_vertex_animation_" in a.name):
                continue
            if ((a.name.casefold() in animation_groups) and (context.active_object.animation_data.action is a)):
                data_add = (a.name, a.name, "Animation to switch to", i)
                animations.append(data_add)
                i+=1
        # now get the rest
        for a in bpy.data.actions:
            if ("_vertex_animation_" in a.name):
                continue
            if (a.name.casefold() in animation_groups):
                data_add = (a.name, a.name, "Animation to switch to", i)
                animations.append(data_add)
                i+=1
                
        if (i == 0):
            animations = [(None, None, None, 0)]
        return(animations)
        
        
    animation: bpy.props.EnumProperty(
                items = populate_animation_list,
                name = "Switch to: " )

    @classmethod
    def poll(cls, context):
        return context.active_object is not None
    
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=512)
        
    def execute(self, context):
        animation = self.animation # for some reason, self.animation was mutating before I replaced it with this... good thing strings are immutable.
        for ob in bpy.data.objects:
            if (ob.type == 'ARMATURE'):
                ob.animation_data.action = bpy.data.actions[animation]
            elif (ob.type == 'MESH') and ((sk := ob.data.shape_keys) is not None):
                try:
                    sk.animation_data.action = bpy.data.actions[animation+"_vertex_animation_"+ob.name]
                except KeyError:
                    pass # no animation for me
            else: #object animation, empties, etc.
                try:
                        ob.animation_data.action = bpy.data.actions[animation+ob.name]
                except KeyError:
                    pass # no animation for me
                    
        bpy.context.scene.frame_start = 0
        bpy.context.scene.frame_end   = ceil(bpy.data.actions[animation].frame_range[1])
        
        return {'FINISHED'}







def register():
    bpy.utils.register_class(process_import)
    bpy.utils.register_class(prepare_export)
    bpy.utils.register_class(mw_swap_animation)


def unregister():
    bpy.utils.unregister_class(process_import)
    bpy.utils.unregister_class(prepare_export)
    bpy.utils.unregister_class(mw_swap_animation)
